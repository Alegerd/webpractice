export class Message {
  id: string;
  user: string;
  text: string;
  likes: number;
  liked: boolean;
  userId: string;

  constructor(
    id: string,
    user: string,
    text: string,
    likes: number,
    liked: boolean,
    userId: string
  ) {
    this.id = id;
    this.user = user;
    this.text = text;
    this.likes = likes;
    this.liked = liked;
    this.userId = userId;
  }
}
