export class User {
  id: string;
  firstName: string;
  lastName: string;
  login: string;
  password: string;
  avatar: string;

  constructor(
    id: string,
    firstName: string,
    lastName: string,
    login: string,
    password: string,
    avatar: string
  ) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.login = login;
    this.password = password;
    this.avatar = avatar;
  }
}
