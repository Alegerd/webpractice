import {Component, OnInit} from '@angular/core';
import {Router, NavigationEnd} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {first} from 'rxjs/operators';
import {UserService} from '../services/user.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  searchForm: FormGroup;
  submitted: boolean;
  noSuchUser: boolean;
  appTitle = 'Twitter';
  currentUrl: string;
  searchPanelVisible: boolean;

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private userService: UserService) {
    router.events.subscribe((_: NavigationEnd) => {
      if (_.url != null) {
        this.currentUrl = _.url;
      }
    });
  }

  ngOnInit() {
    this.searchForm = this.formBuilder.group({
      username: ['', Validators.required]
    });
  }

  public findUserBtnClicked() {
    this.noSuchUser = false;
    this.searchPanelVisible = !this.searchPanelVisible;
  }

  onSubmit() {
    this.submitted = true;
    if (this.searchForm.invalid) {
      return;
    }
    this.userService.getUser(this.searchForm.controls.username.value)
      .pipe(first())
      .subscribe(user => {
        if (user.length > 0) {
          this.noSuchUser = false;
          this.router.navigateByUrl('/', {skipLocationChange: true}).then(() =>
            this.router.navigate(['/profile'], {queryParams: {user: user[0].login}}));
        } else {
          this.noSuchUser = true;
        }
      });
  }

  goToProfile() {
    this.searchPanelVisible = false
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() =>
      this.router.navigate(['/profile']));
  }
}
