import {Component, OnInit} from '@angular/core';
import {UserService} from '../services/user.service';
import {MessageService} from '../services/message.service';
import {User} from '../classes/User';
import {Message} from '../classes/Message';
import {AuthenticationService} from '../services/authentication.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {first} from 'rxjs/operators';
import {Router, ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-progile',
  templateUrl: './progile.component.html',
  styleUrls: ['./progile.component.scss']
})
export class ProgileComponent implements OnInit {

  messageForm: FormGroup;
  private currentUser: User;
  private pickedUsername: string;
  private pickedUserId: string;
  private messages: Message[];
  private followed: string[];
  submitted = false;

  constructor(private userService: UserService,
              private messageService: MessageService,
              private route: ActivatedRoute,
              private authenticationService: AuthenticationService,
              private router: Router,
              private formBuilder: FormBuilder) {
    this.pickedUsername = authenticationService.getCurrentUsername();
  }

  ngOnInit() {
    this.followed = [];
    this.pickedUsername = this.route.snapshot.queryParams['user'] || '';
    this.pickedUserId = this.route.snapshot.queryParams['userId'];
    this.getUserInfo();
    this.messageForm = this.formBuilder.group({
      content: ['', Validators.required]
    });
  }

  onSubmit() {
    this.submitted = true;
    if (this.messageForm.invalid) {
      return;
    }
    this.messageService.postNewMessage(this.messageForm.controls.content.value)
      .pipe(first())
      .subscribe(data => {
        this.getUserMessages();
      });
  }

  public getUserInfo() {
    if (this.pickedUserId != null) {
      this.userService.getUserById(this.pickedUserId).subscribe(user => {
        const currentUser = user;
        if (currentUser.avatar == null || currentUser.avatar === '') {
          currentUser.avatar = 'https://1001freedownloads.s3.amazonaws.com/vector/thumb/75167/1366695174.png';
        }
        this.currentUser = currentUser;
        this.getUserMessages();
        this.getFollowed();
      });
    } else {
      if (this.pickedUsername === '') {
        this.userService.getLoggedInUser().subscribe(user => {
          const currentUser = user;
          if (currentUser.avatar == null || currentUser.avatar === '') {
            currentUser.avatar = 'https://1001freedownloads.s3.amazonaws.com/vector/thumb/75167/1366695174.png';
          }
          this.currentUser = currentUser;
          this.getUserMessages();
          this.getFollowed();
        });
      } else {
        this.userService.getUser(this.pickedUsername).subscribe(user => {
          const currentUser = user[0];
          if (currentUser.avatar == null || currentUser.avatar === '') {
            currentUser.avatar = 'https://1001freedownloads.s3.amazonaws.com/vector/thumb/75167/1366695174.png';
          }
          this.currentUser = currentUser;
          this.getUserMessages();
          this.getFollowed();
        });
      }
    }
  }

  private getFollowed() {
    this.userService.getFollowed().subscribe((users) => {
      console.log(users);
      this.followed = users.map(user => user.login);
    });
  }

  public getUserMessages() {
    this.messageService.getUserMessages(this.currentUser.id).subscribe(messages => {
      console.log(messages);
      this.messages = messages;
    });
  }

  public likeBtnPressed(messageId: string, liked: boolean) {
    if (!liked) {
      this.messageService.addLike(messageId).subscribe(() => {
        this.getUserMessages();
      });
    } else {
      this.messageService.removeLike(messageId).subscribe(() => {
        this.getUserMessages();
      });
    }
  }

  public follow(user: string) {
    this.userService.follow(user).subscribe(() => {
      this.getUserInfo();
      this.getFollowed();
    });
  }

  public unfollow(user: string) {
    this.userService.unfollow(user).subscribe(() => {
      this.getUserInfo();
      this.getFollowed();
    });
  }

  public isFollowed(username: string) {
    return this.followed.includes(username);
  }

  public navigateToUser(userId: string) {
    this.router.navigate(['/profile'], { queryParams: { userId: userId } });
  }
}
