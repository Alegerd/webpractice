import {Injectable} from '@angular/core';
import {Message} from '../classes/Message';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  constructor(private http: HttpClient) {
  }

  public getUserMessages(userId: string) {
    return this.http.get<Message[]>(
      `https://twatter-project.herokuapp.com/api/messages`,
      {
        params: {
          user: userId
        }
      });
  }

  public getSubscribedMessages() {
    return this.http.get<Message[]>(`https://twatter-project.herokuapp.com/api/messages`);
  }

  public postNewMessage(text: string) {
    return this.http.post('https://twatter-project.herokuapp.com/api/messages', text);
  }

  public addLike(message: string) {
    return this.http.post('https://twatter-project.herokuapp.com/api/likes', message);
  }

  public removeLike(message: string) {
    return this.http.post('https://twatter-project.herokuapp.com/api/likes/delete', message);
  }


}
