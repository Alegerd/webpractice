import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';

@Injectable({providedIn: 'root'})
export class AuthenticationService {
  constructor(private http: HttpClient) {
  }

  login(login: string, password: string) {
    return this.http.post<any>(`https://twatter-project.herokuapp.com/auth/login`, {login, password})
      .pipe(map(user => {
        if (user) {
          user.authdata = window.btoa(login + ':' + password);
          localStorage.setItem('currentUser', JSON.stringify(user));
          localStorage.setItem('currentUsername', login);
        }

        return user;
      }));
  }

  signUp(login: string, password: string, firstName: string, lastName: string) {
    return this.http.post<any>(`https://twatter-project.herokuapp.com/auth`, {login, password, firstName, lastName})
      .pipe(map(user => {
        if (user) {
          user.authdata = window.btoa(login + ':' + password);
          localStorage.setItem('currentUser', JSON.stringify(user));
          localStorage.setItem('currentUsername', login);
        }

        return user;
      }));
  }

  logout() {
    localStorage.removeItem('currentUser');
  }

  getCurrentUsername(): string {
    return localStorage.getItem('currentUsername');
  }
}
