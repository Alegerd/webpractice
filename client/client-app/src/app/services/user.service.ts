import {Injectable} from '@angular/core';
import {User} from '../classes/User';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) {
  }

  public getUser(username: string) {
    if (username === '') {
      return this.http.get<User[]>(
        `https://twatter-project.herokuapp.com/api/user`);
    }
    return this.http.get<User[]>(
      `https://twatter-project.herokuapp.com/api/user/search`,
      {
        params: {
          search: username
        }
      });
  }

  public getUserById(userId: string) {
    return this.http.get<User>(
      `https://twatter-project.herokuapp.com/api/user/` + userId );
  }

  public getLoggedInUser() {
    return this.http.get<User>(
      `https://twatter-project.herokuapp.com/api/user`);
  }

  public getFollowed() {
    return this.http.get<User[]>(`https://twatter-project.herokuapp.com/api/followers`);
  }

  public follow(userId: string) {
    return this.http.post(`https://twatter-project.herokuapp.com/api/followers`, userId);
  }

  public unfollow(userId: string) {
    return this.http.post('https://twatter-project.herokuapp.com/api/followers/delete', userId);
  }
}
