import {Component, OnInit} from '@angular/core';
import {MessageService} from '../services/message.service';
import {User} from '../classes/User';
import {Message} from '../classes/Message';
import {UserService} from '../services/user.service';
import {AuthenticationService} from '../services/authentication.service';
import {Router, ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  private messages: Message[];
  private currentUsername: string;

  constructor(private messageService: MessageService,
              private userService: UserService,
              private router: Router,
              private authentivationService: AuthenticationService) {
    this.currentUsername = authentivationService.getCurrentUsername();
  }

  ngOnInit() {
    this.getSubscribedMessages();
  }

  public getSubscribedMessages() {
    this.messageService.getSubscribedMessages().subscribe(messages => {
      this.messages = messages;
    });
  }

  public navigateToUser(userId: string) {
    this.router.navigate(['/profile'], { queryParams: { userId: userId } });
  }
}
